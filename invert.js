module.exports = function invert(obj) {
    let newObj = {};
    for (let key in obj) {
        newObj[obj[key]] = key;
    };
    return newObj;
}