module.exports = function defaults(obj, defaultProps) {
    for (let properties in defaultProps) {
        if (!Object.prototype.hasOwnProperty(obj, properties)){
            obj[properties] = defaultProps[properties];
        }       
    }
    return obj;
}
