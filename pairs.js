module.exports = function pairs(obj) {
    let pairArray = [];
    for (let property in obj) {
        pairArray.push([property, obj[property]]);
    }
    return pairArray;
}