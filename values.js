module.exports = function values(obj) {
    var valArray = [];
    for (let value in obj) {
        valArray.push(obj[value]);
    }
    return valArray;
}