module.exports = function keys(obj) {
    var keyArray = [];
    for (let property in obj) {
        keyArray.push(property);
    }
    return keyArray;
}