const mapObject = require('../mapObject.js');
const testObject = require('../testObject.js');
let cb = a => a.toString();
let result = mapObject(testObject, cb);
let expectedResult = { name: 'Bruce Wayne', age: '36', location: 'Gotham' };
if (JSON.stringify(result) === JSON.stringify(expectedResult)) {
    console.log(result);
}
else {
    console.log("Resuls are different.");
}