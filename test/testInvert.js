const invert = require('../invert.js');
const testObject = require('../testObject.js');
let result = invert(testObject);
let expectedResult = { 'Bruce Wayne': 'name', '36': 'age', 'Gotham': 'location' };
if (JSON.stringify(result) === JSON.stringify(expectedResult)) {
    console.log(result);
}
else {
    console.log("Resuls are different.");
}