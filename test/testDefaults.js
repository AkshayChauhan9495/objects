const defaults = require('../defaults.js');
const testObject = require('../testObject.js');
let props = { "work": "superhero", sidekick: 'robin' };
let result = defaults(testObject, props);
let expectedResult = { name: 'Bruce Wayne', age: 36, location: 'Gotham', work: 'superhero', sidekick: 'robin' };
if (JSON.stringify(result) === JSON.stringify(expectedResult)) {
    console.log(result);
}
else {
    console.log("Resuls are different.");
}